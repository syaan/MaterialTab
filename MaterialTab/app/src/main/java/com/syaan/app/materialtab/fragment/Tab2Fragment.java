package com.syaan.app.materialtab.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.syaan.app.materialtab.MainActivity;
import com.syaan.app.materialtab.R;
import com.syaan.app.materialtab.Sub1Activity;
import com.syaan.app.materialtab.Sub2Activity;

public class Tab2Fragment extends Fragment {

    private Button btnSub1, btnSub2, btnDial;
    private String strIntent;
    private EditText txtIntent;

    public Tab2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab2, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnSub1 = (Button)getView().findViewById(R.id.btn_activity_sub_1);
        btnSub2 = (Button)getView().findViewById(R.id.btn_activity_sub_2);
        btnDial = (Button)getView().findViewById(R.id.btn_activity_dial);

        txtIntent = (EditText)getView().findViewById(R.id.text_Intent);

        btnSub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Sub1Activity.class);
                startActivity(intent);
            }
        });

        btnSub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strIntent = txtIntent.getText().toString();
                Intent intent = new Intent(getActivity(), Sub2Activity.class);
                intent.putExtra(Sub2Activity.KEY_DATA, strIntent);
                startActivityForResult(intent, 0);
            }
        });

        btnDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:082121212093"));
                startActivity(intent);
            }
        });
    }
}