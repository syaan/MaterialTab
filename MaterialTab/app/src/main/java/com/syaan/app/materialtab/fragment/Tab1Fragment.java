package com.syaan.app.materialtab.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.syaan.app.materialtab.R;

public class Tab1Fragment extends Fragment {

    private EditText edtPanjang, edtLebar;
    private Button btnHitung;
    private TextView txtLuas;

    public Tab1Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_tab1, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        edtPanjang = (EditText)getView().findViewById(R.id.edt_panjang);
        edtLebar = (EditText)getView().findViewById(R.id.edt_lebar);
        btnHitung = (Button)getView().findViewById(R.id.btn_hitung);
        txtLuas = (TextView)getView().findViewById(R.id.txt_luas);

        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(edtPanjang.getText()) || TextUtils.isEmpty(edtLebar.getText())) {
                    Toast.makeText(getContext(), "Nilai yang dimasukan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else {
                    String panjang = edtPanjang.getText().toString().trim();
                    String lebar = edtLebar.getText().toString().trim();

                    double p = Double.parseDouble(panjang);
                    double l = Double.parseDouble(lebar);
                    double luas = p * l;
                    txtLuas.setText("Luas : "+luas);
                }
            }
        });
    }

}